<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー新規登録</title>
</head>
<body>
	<h1>ユーザー新規登録</h1>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="signup" method="post">
			<br /> <label for="name">名前</label><br> <input name="name"
				id="name" />（名前はあなたの公開プロフィールに表示されます）<br /> <label for="login_id">ID</label><br>
			<input name="login_id" id="login_id" /> <br /> <label
				for="password">パスワード</label><br> <input name="password"
				type="password" id="password" /> <br /> <label for="password">パスワード確認用</label><br>
			<input name="password2" type="password" id="password" /> <br />



			<%--murataさんbranch --%>
			<select name="branch">
				<%--部品の名前がbranch --%>
				<c:forEach items="${branches}" var="branch">
					<option value="${branch.id}">${branch.branch_name}</option>
				</c:forEach>
			</select>


			<%--department_name --%>
			<select name="department">
				<c:forEach items="${departmentes}" var="department">
					<option value="${department.id}">${department.department_name}</option>
				</c:forEach>
			</select> <br /> <input type="submit" value="登録" /> <br /> <a
				href="management">戻る</a>
		</form>
	</div>
</body>
</html>