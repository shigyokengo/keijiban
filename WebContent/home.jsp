<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム画面</title>
</head>

<body>
	<!-- loginUserが空 -->
	<c:if test="${ empty loginUser }">
		<a href="login">ログイン</a>

	</c:if>

	<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

	<c:if test="${ not empty loginUser }">

		<a href="management">設定</a>
		<a href="newMessage">新規投稿</a>

		<%--post追加 --%>
		<a href="logout">ログアウト</a>


		<div class="name">
			<h2>
				<c:out value="${loginUser.name}" />
			</h2>
		</div>


		<div class="main-contents">

		<div class="post">

		<form action="./" method="get">
						 <p>検索したいキーワードを入力してください。</p>
  						<input type="searchcategory" name="searchcategory" placeholder="カテゴリーを入力">




<!-- a期間絞込み -->
		<!-- <form action="./" method="get" target="_blank"> -->

			<p>
			<label>
			日付入力欄：
			<input type="date" name="period1">
			<input type="date" name="period2">
			</label>
			</p>
			<input type="submit" name="submit" value="検索">
		</form>



				<c:forEach items="${post}" var="post">
						<%--a 本文削除 --%>

						<c:if test="${ post.status == 0 }">
						<h1 class="cp_h1title">
						<span class="name"><c:out value="${post.name}" /></span>
						</h1>


						<div class="text">
						件名：<p><c:out value="${post.subject}" /></p>
						カテゴリ：<p><c:out value="${post.category}" /></p>
						本文：<p><c:out value="${post.text}" /></p>
						</div>
							<div class="date">
							<fmt:formatDate value="${post.created_date}"
								pattern="yyyy/MM/dd HH:mm:ss" />
						</div>



						<c:if test="${ post.userId == loginUser.id }">
						<form action="deleteMessage" method="post">
							<button class="btn" type="submit" name="status" value="1"
											onClick="return activeDialog()">本文削除</button>
											<input type="hidden" name="post.id" value="${post.id}">
						</form>
						</c:if>




						<c:forEach items="${comment}" var="comment">


							<%--aコメント表示 --%>
							<c:if test="${ comment.post_id == post.id }">

								<c:if test="${ comment.status == 0 }">

										名前：<span class="name"><c:out value="${comment.name}" /></span>

										<br>
										<c:out value="${comment.text}" />
										<c:out value="${comment.createdDate}" />

					<c:if test="${ comment.userId == loginUser.id }">
										<form action="deletecomment" method="post">
										<button class="btn" type="submit" name="status" value="1"
											onClick="return activeDialog()">コメント削除</button>
										<input type="hidden" name="id" value="${comment.id}">
										</form>

								</c:if>
							</c:if>
							</c:if>
						</c:forEach>


						<%--comment 　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　--%>
						<form action="commentServlet" method="post">
							<textarea name="text" cols="50" rows="4" class="tweet-box"></textarea>
							<br /> <input type="submit" value="コメント"> <input
								type="hidden" name="post_id" value="${post.id}">
						</form>
</c:if>


				</c:forEach>
			</div>


		</div>
 </c:if>
</body>
</html>
