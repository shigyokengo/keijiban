<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${loginUser.login_id}の設定</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="settings" method="post">
			<br /> <input name="id" value="${editUser.id}" id="id" type="hidden" />
			<label for="name">名前</label> <input name="name"
				value="${editUser.name}" id="name" />（名前はあなたの公開プロフィールに表示されます）<br />

			<label for="login_id">ログイン名</label> <input name="login_id"
				value="${editUser.login_id}" /><br /> <label for="password">パスワード</label>
			<input name="password" type="password" id="password" /> <br /> <label
				for="password">パスワード確認用</label> <input name="password2"
				type="password" id="password" /> <br />




			<%--gawadakesakusei --%>
			<select name="branch">
				<%--部品の名前がbranch --%>
				<c:forEach items="${branches}" var="branch">
					<option value="${branch.id}">${branch.branch_name}</option>
					<%--ここだよ --%>
				</c:forEach>
			</select> <select name="department">
				<c:forEach items="${departmentes}" var="department">
					<option value="${department.id}">${department.department_name}</option>
				</c:forEach>
			</select> <input type="submit" value="登録" /> <br /> <a href="./">戻る</a>
		</form>
		<div class="copyright">Copyright(c)Your Name</div>
	</div>
</body>
</html>