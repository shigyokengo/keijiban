<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<script>
	function activeDialog() {
		var txt;
		var rt = confirm("本当に復活させますか？");
		if (rt == true) {
			txt = "「ok」を選択しました。";
			return true;
		} else {
			txt = "「キャンセル」を選択しました。";
			return false;
		}
		document.getElementById("conf1").innerHTML = txt;
	}
	function stopDialog() {
		var txt;
		var rt = confirm("本当に停止させますか？");
		if (rt == true) {
			txt = "「ok」を選択しました。";
			return true;
		} else {
			txt = "「キャンセル」を選択しました。";
			return false;
		}
		document.getElementById("conf2").innerHTML = txt;
	}
</script>
<body>
	<div class="header">
		<a href="./">home</a> <a href="signup">ユーザー新規登録</a>
	</div>


	<table border="1">
		<tr>
			<th>名 称</th>
			<th>支 店</th>
			<th>部 署</th>
			<th>設 定</th>
		</tr>

		<%--manegsabure --%>
		<c:forEach items="${users}" var="user">
			<tr>
				<td><c:out value="${user.name}" /></td>
				<td><c:out value="${user.branch_name}" /></td>
				<td><c:out value="${user.department_name}" /></td>





				<td>
					<form action="management" method="post">
						<c:if test="${ user.stop == 1 }">
							<button class="btn" type="submit" name="stop" value="0"
								onClick="return activeDialog()">復活</button>
							<input type="hidden" name="id" value="${user.id}">
						</c:if>

						<c:if test="${ user.stop == 0 }">
							<button class="btn" type="submit" name="stop" value="1"
								onClick="return stopDialog()">停止</button>
							<input type="hidden" name="id" value="${user.id}">
						</c:if>
					</form>
				</td>

				<td><a href="settings?userid=${user.id}">編集</a></td>

			</tr>
		</c:forEach>

	</table>


</body>
</html>