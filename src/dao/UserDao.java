package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import beans.Department;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();//StringBuilderは、一言でいうと、Stringクラス同様、文字列を扱うクラスです
			sql.append("INSERT INTO users ( ");
			sql.append(" login_id");//DBusersのカラム名指定する
			sql.append(", name");
			sql.append(", password");
			sql.append(",branch");
			sql.append(",department");
			sql.append(",status");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");//VALUES は、値の式で指定された行あるいは行の集合を計算します。
			sql.append("?");
			sql.append(",?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLogin_id());///beansにセットしてある値を持ってきてDBにsetする順番一緒にする
			ps.setString(2, user.getName());
			ps.setString(3, user.getPassword());
			ps.setInt(4, user.getBranch());
			ps.setInt(5, user.getDepartment());
			ps.setInt(6, user.getStop());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//j ログインidとパスワードを引数を渡す
	public User getUser(Connection connection, String login_id,
			String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";
			////////login_idとpasswordを？にセットする  条件にあったlogin_idとpasswordの一人の全部の情報アスタリスクで
			ps = connection.prepareStatement(sql);
			ps.setString(1, login_id);//a 引数で持ってきたものを?にセットする
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();//a 一人の情報テーブルができるrsにいれる
			List<User> userList = toUserList(rs);//toUserListにとぶ 帰ってきたらuserListに入れる

			//List化しているため要素数がある
			if (userList.isEmpty() == true) {//a  isEmptyで空かどうかチェックできる
				return null;
			} else if (2 <= userList.size()) {//while回っているから要素数が2以上になることない
				throw new IllegalStateException("2 <= userList.size()");
			} else {//userListに入っているよう素数0のものを返す
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();// retのarraylist作成これから中にいれる
		try {
			while (rs.next()) {
				int id = rs.getInt("id");// 上で条件にあった一人分の情報がrsに入っているからそれを変数化
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				String password = rs.getString("password");
				int branch = rs.getInt("branch");
				int department = rs.getInt("department");
				int stop = rs.getInt("status");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");

				//a 変数化したものをuserbeanseに入れていきたい
				User user = new User();//Userクラスのuserに上で変数化したものをsetする
				user.setId(id);
				user.setLogin_id(login_id);
				user.setName(name);
				//user.setEmail(email);
				user.setPassword(password);
				user.setBranch(branch);
				user.setDepartment(department);
				user.setStop(stop);//
				user.setCreatedDate(createdDate);
				user.setUpdatedDate(updatedDate);

				ret.add(user);//userリストに入れる
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	//murataさん追加分　 branchdaoだよ

	public List<Branch> getBranches(Connection conn) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM branch";//DBのbranch全て選ぶ
			ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();//実行
			return toBranchList(rs);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Branch> toBranchList(ResultSet rs) throws SQLException {
		List<Branch> ret = new ArrayList<>();

		while (rs.next()) {
			int id = rs.getInt("id");//DBのカラムidを変数化
			String name = rs.getString("branch_name");

			Branch branch = new Branch();////branchのbeanseに入れたいからインスタンス生成
			branch.setId(id);///branchのbeanseにセット
			branch.setBranch_name(name);

			ret.add(branch);//retにbranchリスト追加
		}

		return ret;
	}

	/////////////depart
	public List<Department> getDepartmentes(Connection conn) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM department";
			ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			return toDepartmentList(rs);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Department> toDepartmentList(ResultSet rs) throws SQLException {
		List<Department> ret = new ArrayList<>();

		while (rs.next()) {
			int id = rs.getInt("id");
			String name = rs.getString("department_name");

			Department department = new Department();
			department.setId(id);
			department.setDepartment_name(name);

			ret.add(department);
		}

		return ret;
	}

	/////jyoukenn SQLwotukuru
	public List<User> getManagement(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("users.password as password, ");
			sql.append("users.department as department, ");
			sql.append("users.branch as branch, ");
			sql.append("users.status as status, ");

			sql.append("department.id as department_id, ");
			sql.append("department.department_name as department_name, ");

			sql.append("branch.id as branch_id, ");
			sql.append("branch.branch_name as branch_name ");

			sql.append("FROM users ");
			sql.append("INNER JOIN department ");
			sql.append("ON users.department = department.id ");
			sql.append("INNER JOIN branch ");
			sql.append("ON users.branch = branch.id ");

			sql.append("ORDER BY id ASC");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<User> ret = toManagementList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//////hennsuuniirenakerebanaranai
	private List<User> toManagementList(ResultSet rs)
			throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String department_name = rs.getString("department_name");
				String branch_name = rs.getString("branch_name");
				int status = rs.getInt("status");

				User management = new User();

				management.setId(id);
				management.setName(name);
				management.setDepartment_name(department_name);
				management.setBranch_name(branch_name);
				management.setStop(status);

				ret.add(management);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	////hennsyuu gamennnoDao
	public User getUser(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  login_id = ?");
			sql.append(", name = ?");
			sql.append(", password = ?");
			sql.append(", branch = ?");
			sql.append(", department = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLogin_id());
			ps.setString(2, user.getName());
			ps.setString(3, user.getPassword());
			ps.setInt(4, user.getBranch());
			ps.setInt(5, user.getDepartment());
			ps.setInt(6, user.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public void stop(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" status = ?");
			sql.append(" WHERE ");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user.getStop());
			ps.setInt(2, user.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

}