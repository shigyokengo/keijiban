package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentdisplayDao {

	public List<Comment> getComment(Connection connection, int num) {
	//a コメントを表示する　
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comment.id as id, ");//comment.idは、beanse as カラム名
			sql.append("comment.user_id as user_id, ");
			sql.append("comment.post_id as post_id, ");
			sql.append("users.name as name, ");
			sql.append("comment.text as text,");
			sql.append("comment.created_date as created_date, ");
			sql.append("comment.status as status ");
			sql.append("FROM comment ");
			sql.append("INNER JOIN users ");
			sql.append("ON comment.user_id = users.id ");//a コメントidとuserのid
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Comment> ret = toCommentList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Comment> toCommentList(ResultSet rs)
			throws SQLException {

		List<Comment> ret = new ArrayList<Comment>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");//DBから抽出したカラムを変数化
				int user_id = rs.getInt("user_id");
				int post_id = rs.getInt("post_id");
				String name = rs.getString("name");
				String text = rs.getString("text");
				Timestamp createdDate = rs.getTimestamp("created_date");
				int status = rs.getInt("status");

				//CommentのbeanseにDBから抽出したものを入れる
				Comment comment = new Comment();
				comment.setId(id);
				comment.setUserId(user_id);
				comment.setPost_id(post_id);
				comment.setName(name);
				comment.setText(text);
				comment.setCreatedDate(createdDate);
				comment.setStatus(status);

				ret.add(comment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}