package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Comment;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class CommentDao {

	//a ユーザーが入力したコメントをDBに追加
	public void insert(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comment ( ");
			sql.append("id");
			sql.append(",user_id");
			sql.append(", text");
			sql.append(",post_id");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append(" ?");//id
			sql.append(", ?");//user_id
			sql.append(", ?");//text
			sql.append(", ?");//post_id
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, comment.getId());//commentのビーンズを追加
			ps.setInt(2, comment.getUserId());
			ps.setString(3, comment.getText());
			ps.setInt(4, comment.getPost_id());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	//a コメント削除
	public void delete(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE comment SET");
			sql.append(" status = ?");
			sql.append(" WHERE ");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, comment.getStatus());
			ps.setInt(2, comment.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	//	public void delete_mescom(Connection connection, Message message) {
	//
	//		PreparedStatement ps = null;
	//		try {
	//			StringBuilder sql = new StringBuilder();
	//			sql.append("UPDATE comment SET");
	//			sql.append(" status = ?");
	//			sql.append(" WHERE ");
	//			sql.append(" post_id = ?");
	//
	//			ps = connection.prepareStatement(sql.toString());
	//
	//			ps.setInt(1, message.getStatus());
	//			ps.setInt(2, message.getId());
	//
	//			int count = ps.executeUpdate();
	//			if (count == 0) {
	//				throw new NoRowsUpdatedRuntimeException();
	//			}
	//		} catch (SQLException e) {
	//			throw new SQLRuntimeException(e);
	//		} finally {
	//			close(ps);
	//		}
	//
	//	}

}