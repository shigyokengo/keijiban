package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> getUserMessages(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("post.id as id, ");//aテーブル名カラム
			sql.append("post.text as text, ");//messagesをpostにデータベース名
			sql.append("post.user_id as user_id, ");//messagesをpostにデータベース名
			sql.append("post.subject as subject, ");
			sql.append("post.category as category, ");
			sql.append("users.login_id as login_id, ");//accountをlogin_id
			sql.append("users.name as name, ");
			sql.append("post.created_date as created_date, ");//messagesをpostにデータベース名
			sql.append("post.status as status ");
			sql.append("FROM post ");//messagesをpostにデータベース名
			sql.append("INNER JOIN users ");
			sql.append("ON post.user_id = users.id ");//messagesをpostにデータベース名
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				String loginId = rs.getString("login_id");//asの後ろと対応させる
				String name = rs.getString("name");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String text = rs.getString("text");
				Timestamp createdDate = rs.getTimestamp("created_date");
				int status = rs.getInt("status");
				String subject = rs.getString("subject");
				String category = rs.getString("category");

				UserMessage message = new UserMessage();
				message.setLogin_Id(loginId);///accountをlogin_id
				message.setName(name);
				message.setId(id);
				message.setUserId(userId);
				message.setText(text);
				message.setCreated_date(createdDate);
				message.setStatus(status);
				message.setSubject(subject);
				message.setCategory(category);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	//a 検索
	public List<UserMessage> getCategory(Connection connection, String category) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("post.id as id, ");//aテーブル名カラム
			sql.append("post.text as text, ");//messagesをpostにデータベース名
			sql.append("post.user_id as user_id, ");//messagesをpostにデータベース名
			sql.append("post.subject as subject, ");
			sql.append("post.category as category, ");
			sql.append("users.login_id as login_id, ");//accountをlogin_id
			sql.append("users.name as name, ");
			sql.append("post.created_date as created_date, ");//messagesをpostにデータベース名
			sql.append("post.status as status ");
			sql.append("FROM post ");//messagesをpostにデータベース名
			sql.append("INNER JOIN users ");
			sql.append("ON post.user_id = users.id ");//messagesをpostにデータベース名
			sql.append("WHERE category LIKE ?");//sqlでLIKE検索詳細
			sql.append("ORDER BY created_date DESC ");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, "%" + category + "%");//categoryが?なのでここに入れるものを引数として持ってきているのでそれをpsにセット
			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toCategoryList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toCategoryList(ResultSet rs)
			throws SQLException {

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				String loginId = rs.getString("login_id");//asの後ろと対応させる
				String name = rs.getString("name");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String text = rs.getString("text");
				Timestamp createdDate = rs.getTimestamp("created_date");
				int status = rs.getInt("status");
				String subject = rs.getString("subject");
				String category = rs.getString("category");

				UserMessage message = new UserMessage();
				message.setLogin_Id(loginId);///accountをlogin_id
				message.setName(name);
				message.setId(id);
				message.setUserId(userId);
				message.setText(text);
				message.setCreated_date(createdDate);
				message.setStatus(status);
				message.setSubject(subject);
				message.setCategory(category);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	///a期間絞込み
	public List<UserMessage> getPeriod(Connection connection, String period_first, String period_end) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("post.id as id, ");//aテーブル名カラム
			sql.append("post.text as text, ");
			sql.append("post.user_id as user_id, ");
			sql.append("post.subject as subject, ");
			sql.append("post.category as category, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("post.created_date as created_date, ");
			sql.append("post.status as status ");
			sql.append("FROM post ");
			sql.append("INNER JOIN users ");
			sql.append("ON post.user_id = users.id ");
			sql.append("WHERE post.created_date BETWEEN ? and ? ");
			sql.append("ORDER BY post.created_date DESC ");

			System.out.println(period_first);
			System.out.println(period_end);
			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, period_first + " 00:00:00");//categoryが?なのでここに入れるものを引数として持ってきているのでそれをpsにセット
			ps.setString(2, period_end + " 23:59:59");
			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toPeriodList(rs);
			System.out.println(ret.size());

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toPeriodList(ResultSet rs)
			throws SQLException {

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				String loginId = rs.getString("login_id");//asの後ろと対応させる
				String name = rs.getString("name");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String text = rs.getString("text");
				Timestamp createdDate = rs.getTimestamp("created_date");
				int status = rs.getInt("status");
				String subject = rs.getString("subject");
				String category = rs.getString("category");
				UserMessage message = new UserMessage();
				message.setLogin_Id(loginId);///accountをlogin_id
				message.setName(name);
				message.setId(id);
				message.setUserId(userId);
				message.setText(text);
				message.setCreated_date(createdDate);
				message.setStatus(status);
				message.setSubject(subject);
				message.setCategory(category);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	//a　日時、検索カテゴリ
	public List<UserMessage> getSerch_datecategory(Connection connection, String period_first, String period_end,
			String category) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("post.id as id, ");//aテーブル名カラム
			sql.append("post.text as text, ");
			sql.append("post.user_id as user_id, ");
			sql.append("post.subject as subject, ");
			sql.append("post.category as category, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("post.created_date as created_date, ");
			sql.append("post.status as status ");
			sql.append("FROM post ");
			sql.append("INNER JOIN users ");
			sql.append("ON post.user_id = users.id ");

			if (!StringUtils.isEmpty(category)) {
				sql.append("WHERE post.created_date BETWEEN ? and ? AND category LIKE? ");
				ps = connection.prepareStatement(sql.toString());
				ps.setString(1, period_first + " 00:00:00");//categoryが?なのでここに入れるものを引数として持ってきているのでそれをpsにセット
				ps.setString(2, period_end + " 23:59:59");
				ps.setString(3, "%" + category + "%");
			} else {
				sql.append("WHERE post.created_date BETWEEN ? and ? ");
				ps = connection.prepareStatement(sql.toString());
				ps.setString(1, period_first + " 00:00:00");//categoryが?なのでここに入れるものを引数として持ってきているのでそれをpsにセット
				ps.setString(2, period_end + " 23:59:59");
			}

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toSerchList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toSerchList(ResultSet rs)
			throws SQLException {

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				String loginId = rs.getString("login_id");//asの後ろと対応させる
				String name = rs.getString("name");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String text = rs.getString("text");
				Timestamp createdDate = rs.getTimestamp("created_date");
				int status = rs.getInt("status");
				String subject = rs.getString("subject");
				String category = rs.getString("category");
				UserMessage message = new UserMessage();
				message.setLogin_Id(loginId);///accountをlogin_id
				message.setName(name);
				message.setId(id);
				message.setUserId(userId);
				message.setText(text);
				message.setCreated_date(createdDate);
				message.setStatus(status);
				message.setSubject(subject);
				message.setCategory(category);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}