/*package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UserService;


@WebFilter(filterName = "authorityfilter", urlPatterns = { "/*" })
public class Errorfilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		//a サーブレットパスを取得
		String path = ((HttpServletRequest) request).getServletPath();

		HttpSession session = ((HttpServletRequest) request).getSession();

		//aログインユーザーを取得して、ログイン情報を変数に入れる
		User loginUser = (User) session.getAttribute("loginUser");

		//aエラメ格納するリスト
		List<String> messages = new ArrayList<String>();

//		if (path.equals("/login")) {
//			chain.doFilter(request, response);
//			return;
//		}

		//aログインしてなきゃトップに飛ばしたい 無限ループ防ぐための処理が&&の前
		if (!path.endsWith("/login") && loginUser == null) {
			messages.add("ログインしてください");
			session.setAttribute("errorMessages", messages);
			((HttpServletResponse) response).sendRedirect("login");
			return;
		}

		//aログインしているユーザーがログイン画面に行ったときホームに飛ばす
		if(path.equals("/login") && loginUser != null) {
			messages.add("既にログインしています");
			session.setAttribute("errorMessages", messages);
			((HttpServletResponse) response).sendRedirect("./");
			return;

		}

        //a権限フィルター
		if (path.equals("/userManage") || path.equals("/userEdit") || path.equals("/signup")) {
			// aログインユーザー情報の取得　idをとる　権限あるか確認する
			User user = new UserService().getUser(loginUser.getId());

			if (user.getDepartment() != 1 || user.getBranch() != 1) {//a支店か役職が権限もってない
				messages.add("アクセス権限がありません");
				session.setAttribute("errorMessages", messages);
				((HttpServletResponse) response).sendRedirect("./");//権限が無ければリダイレクトさせる

				return;
			}
		}

		//a停止中かどうかのチェック
		if (loginUser != null) {
			User user = new UserService().getUser(loginUser.getId());
			if (user.getIsStopped() == 1) {
				messages.add("このユーザーは停止されています");

				session.setAttribute("errorMessages", messages);
				session.removeAttribute("loginUser"); //aログイン情報消す

				((HttpServletResponse) response).sendRedirect("login"); //aログインページへ飛ばしたい
				return;
			}
		}

		chain.doFilter(request, response);
	}


	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void destroy() {
	}
}*/