package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Message;
import service.MessageService;

@WebServlet("/deleteMessage")
public class DeleteMessage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Message message = new Message();

		message.setStatus(Integer.parseInt(request.getParameter("status")));
		message.setId(Integer.parseInt(request.getParameter("post.id")));
		//
		new MessageService().delete_mes(message);
		//a投稿に対してのコメントを全部消す
		//new CommentService().delete_mescom(message);
		response.sendRedirect("./");

	}

}
