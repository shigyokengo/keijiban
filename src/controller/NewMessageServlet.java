//a 新規投稿
package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("post.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();///セッション開始

		List<String> post = new ArrayList<String>();//post.jspのリクエストをpost変数にまとめたい

		if (isValid(request, post) == true) {//postで送られたものをrequestとlistのpostを送る

			//loginservletでセッションスコープのloginUserをsetAttributeされたものを利用
			User user = (User) session.getAttribute("loginUser");//セッションスコープからインスタンスを取得

			Message message = new Message();//post.jspからもらったパラメーターをmessage.beanseに
			message.setText(request.getParameter("message"));//requestに入っているjspからのパラメーターをmessageのbeanseに
			message.setCategory(request.getParameter("category"));
			message.setSubject(request.getParameter("subject"));
			message.setUserId(user.getId());////userのIDがほしいから上でセッションを取得したそれをmessageのbeanseに入れる

			new MessageService().register(message);

			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", post);
			response.sendRedirect("./");
		}
	}

	//a ユーザーが件名、カテゴリ、本文を入力確認
	private boolean isValid(HttpServletRequest request, List<String> post) {

		String message = request.getParameter("message");//入力されたパラメーターを変数化する
		String category = request.getParameter("category");//requestに入っている
		String subject = request.getParameter("subject");

		if (StringUtils.isEmpty(message) == true) {//a 本文入力確認
			post.add("メッセージを入力してください");
		}
		if (1000 < message.length()) {//a 本文1000文字以下かを確認
			post.add("1000文字以下で入力してください");
		}

		if (StringUtils.isEmpty(category) == true) {//a カテゴリー入力確認
			post.add("カテゴリーを入力してください");
		}

		if (StringUtils.isEmpty(subject) == true) {//a 件名入力確認
			post.add("件名を入力してください");
		}

		if (10 < category.length()) {//a カテゴリー10文字以下かを確認
			post.add("カテゴリー欄10文字以下で入力してください");
		}

		if (30 < subject.length()) {//a 件名30文字以下かを確認
			post.add("件名欄30文字以下で入力してください");
		}

		if (post.size() == 0) {//postのリストにひとつも入っていない場合true
			return true;
		} else {
			return false;
		}
	}

}