package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		User user = (User) request.getSession().getAttribute("loginUser");
		HttpSession session = request.getSession();
		List<String> post = new ArrayList<String>();
		if (user.getBranch() == 4 && user.getDepartment() == 1) {

			//muratasann　プルダウンで登録
			//Userserviceのgetbranchesに飛びたい
			UserService service = new UserService();//インスタンスの生成
			//UserServiceのクラス型のservice変数に入れる。UserServiceのメソッド使用可能
			request.setAttribute("branches", service.getBranches());//リクエストスコープにインスタンスを保存
			//"branches"がキー getでUserServiseのメソッドを使用する
			//zokuseimei ,instance)
			request.setAttribute("departmentes", service.getDepartmentes());

			request.getRequestDispatcher("signup.jsp").forward(request, response);



			//usersに入ったインスタンスをリクエストスコープに保存
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		} else {

			post.add("本社総務部向けの機能です");
			session.setAttribute("errorMessages", post);
			request.getRequestDispatcher("./").forward(request, response);
		}

//		//muratasann　プルダウンで登録
//		//Userserviceのgetbranchesに飛びたい
//		UserService service = new UserService();//インスタンスの生成
//		//UserServiceのクラス型のservice変数に入れる。UserServiceのメソッド使用可能
//		request.setAttribute("branches", service.getBranches());//リクエストスコープにインスタンスを保存
//		//"branches"がキー getでUserServiseのメソッドを使用する
//		//zokuseimei ,instance)
//		request.setAttribute("departmentes", service.getDepartmentes());
//
//		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();//リストmessages作成

		HttpSession session = request.getSession();//セッション取得したい宣言
		if (isValid(request, messages) == true) {//isValidメソッドに飛ぶ引数はrequestとmessages

			User user = new User();//チェックおｋだったのでユーザーのbeanseにパラメーターをセットする
			user.setName(request.getParameter("name"));////"name"はjspのnameのやつ
			user.setLogin_id(request.getParameter("login_id"));
			user.setPassword(request.getParameter("password"));
			user.setBranch(Integer.parseInt(request.getParameter("branch")));
			user.setDepartment(Integer.parseInt(request.getParameter("department")));

			new UserService().register(user);

			response.sendRedirect("management");
		} else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("signup");
		}
	}

	///a ちゃんとＩＤとパスワードを入力したかチェックする
	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String name = request.getParameter("name");
		String login_id = request.getParameter("login_id");//a jspで入力されたパラメーターを変数化
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");

		if (!password.matches("[-_@+*;:#$%&A-Za-z0-9]+")) {
			if (!password.equals(password2)) {
				messages.add("パスワードは同じものを二度入力してください");
			}

			if (6 > password.length() && 20 < password.length()) {
				messages.add("20文字以下で入力してください");
			}
			if (6 > login_id.length() && 20 < login_id.length()) {
				messages.add("1000文字以下で入力してください");
			}
			if (!(login_id.matches("[a-zA-Z0-9]+")) == true) {
				messages.add("ログインidは半角英数字で入力してください");
			}

			if (10 < name.length()) {
				messages.add("10文字以下で入力してください");
			}

			if (!(password.matches(".*[^\\\\u0020-\\\\u007E].*")) == false) {
				messages.add("パスワードは記号を含む全ての半角文字で入力してください");
			}

			if (StringUtils.isEmpty(name) == true && StringUtils.isEmpty(name) == true) {//passwordが空かどうか
				messages.add("名前を入力してください");
			}
			if (StringUtils.isEmpty(login_id) == true) {//login_id空かどうか
				messages.add("アカウント名を入力してください");
			}
			if (StringUtils.isEmpty(password) == true && StringUtils.isEmpty(password2) == true) {//passwordが空かどうか
				messages.add("パスワードを入力してください");
			}
		}
		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}

	}
}