package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;//追加

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import beans.UserMessage;//追加
import service.CommentService;
import service.MessageService;//追加

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		//userインスタンスの取得・
		User user = (User) request.getSession().getAttribute("loginUser");
		boolean isShowMessageForm;
		if (user != null) {
			isShowMessageForm = true;
		} else {
			isShowMessageForm = false;
		}

		//a 本文
		//List<UserMessage> post = new MessageService().getMessage();

		//a コメントを表示する
		List<Comment> comments = new CommentService().getComment();


		//aカテゴリのパラメーターを取得
		String category = request.getParameter("searchcategory");

		//a サーチしたものをList化する　　　　　　　　　　　　　　//パラメーターを引数としてgetserchに送る
		//List<UserMessage> searchcategoryList = new MessageService().getSearch(category);

		//a 日付検索のパラメーターを取得
		String period_first = request.getParameter("period1");
		String period_end = request.getParameter("period2");
		String old = "2019/04/01" + "00:00:00";//a最初から決めている日付

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		String str = sdf.format(cal.getTime());//今日の日付

		if (!StringUtils.isEmpty(period_first) && !StringUtils.isEmpty(period_end)) {
			//a firstあり endあり
			List<UserMessage> periodList = new MessageService().getSerch_datecategory(period_first, period_end,
					category);
			//a 日付絞込み
			request.setAttribute("post", periodList);

			//a コメント表示jsp表示のためkey,comment
			request.setAttribute("comment", comments);

		} else if (!StringUtils.isEmpty(period_first) && StringUtils.isEmpty(period_end)) {
			//firstあり、endなしは、endにstr
			List<UserMessage> periodList = new MessageService().getSerch_datecategory(period_first, str, category);
			//a 日付絞込み
			request.setAttribute("post", periodList);

			//a コメント
			request.setAttribute("comment", comments);

		} else if (StringUtils.isEmpty(period_first) && !StringUtils.isEmpty(period_end)) {
			//firstなし、endありは、firstにold
			List<UserMessage> periodList = new MessageService().getSerch_datecategory(old, period_end, category);
			//a 日付絞込み
			request.setAttribute("post", periodList);

			//a コメント表示jspのため
			request.setAttribute("comment", comments);

		} else {
			List<UserMessage> periodList = new MessageService().getSerch_datecategory(old, str, category);

			//		    //a コメント
			//a 本文
			request.setAttribute("post", periodList);
			//a コメント表示jspのため
			request.setAttribute("comment", comments);
		}

		request.setAttribute("isShowMessageForm", isShowMessageForm);
		request.getRequestDispatcher("home.jsp").forward(request, response);

	}

}
