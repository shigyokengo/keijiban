package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/commentServlet" })
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();//セッション開始

		List<String> comments = new ArrayList<String>();//commentsのリストに入れる

		if (isValid(request, comments) == true) {//homeからコメントのリクエストくる

			//aログインユーザー情報がほしい誰の投稿のコメントかをリンク
			User user = (User) session.getAttribute("loginUser");
			//comment beanseにtext,post_id,getId
			Comment comment = new Comment();
			comment.setText(request.getParameter("text"));
			//commentのpost_id
			comment.setPost_id(Integer.parseInt(request.getParameter("post_id")));
			comment.setUserId(user.getId());//a 投稿者のid

			new CommentService().register(comment);////a comentserviceにいく

			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", comments);
			response.sendRedirect("./");
		}
	}

	//kコメントの文字数確認するため
	private boolean isValid(HttpServletRequest request, List<String> comments) {

		String comment = request.getParameter("text");//パラメーターを変数化

//				if (StringUtils.isEmpty(comment) == true) {
//					comments.add("メッセージを入力してください");
//				}
		if (500 < comment.length()) {
			comments.add("500文字以下で入力してください");
		}
		if (comments.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}