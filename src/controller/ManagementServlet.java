package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> post = new ArrayList<String>();
		User user = (User) request.getSession().getAttribute("loginUser");
		boolean isShowMessageForm;
		if (user != null) {
			isShowMessageForm = true;
		} else {
			isShowMessageForm = false;
		}
		List<User> users = new UserService().getManagement();//UserServiceのManagementメソッド呼ぶ

		request.setAttribute("users", users);
		request.setAttribute("isShowMessageForm", isShowMessageForm);

		if (user.getBranch() == 4 && user.getDepartment() == 1) {
			//usersに入ったインスタンスをリクエストスコープに保存
			request.getRequestDispatcher("management.jsp").forward(request, response);
		} else {

			post.add("本社総務部向けの機能です");
			session.setAttribute("errorMessages", post);
			request.getRequestDispatcher("./").forward(request, response);
		}
	}

	////a SQLに0か１を
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		User users = new User();

		users.setStop(Integer.parseInt(request.getParameter("stop")));

		users.setId(Integer.parseInt(request.getParameter("id")));

		new UserService().stop(users);
		//request.setAttribute("users", users);
		response.sendRedirect("./management");
	}

}