package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		//HttpSession session = request.getSession();

		//User loginUser = (User) session.getAttribute("loginUser");
		//aセッションよりログインユーザーの情報を取得
		//aログインユーザー情報のidを元にDBからユーザー情報取得
		User user = (User) request.getSession().getAttribute("loginUser");
		HttpSession session = request.getSession();
		List<String> post = new ArrayList<String>();
		if (user.getBranch() == 4 && user.getDepartment() == 1) {
			//usersに入ったインスタンスをリクエストスコープに保存
			User editUser = new UserService().getUser(Integer.parseInt(request.getParameter("userid")));//userNo
			request.setAttribute("editUser", editUser);
			UserService service = new UserService();
			request.setAttribute("branches", service.getBranches());
			request.setAttribute("departmentes", service.getDepartmentes());
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		} else {

			post.add("本社総務部向けの機能です");
			session.setAttribute("errorMessages", post);
			request.getRequestDispatcher("./").forward(request, response);
		}

//		User editUser = new UserService().getUser(Integer.parseInt(request.getParameter("userid")));//userNo
//		request.setAttribute("editUser", editUser);
//		UserService service = new UserService();
//		request.setAttribute("branches", service.getBranches());
//		request.setAttribute("departmentes", service.getDepartmentes());

	}

	/////hennsyuu
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);

		if (isValid(request, messages) == true) {

			try {
				new UserService().update(editUser);
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("settings.jsp").forward(request, response);
				return;
			}

			session.setAttribute("loginUser", editUser);

			response.sendRedirect("management");
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLogin_id(request.getParameter("login_id"));
		editUser.setName(request.getParameter("name"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setBranch(Integer.parseInt(request.getParameter("branch")));
		editUser.setDepartment(Integer.parseInt(request.getParameter("department")));
		//editUser.setDescription(request.getParameter("description"));
		return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");

		if (!password.equals(password2)) {
			messages.add("パスワードは同じものを二度入力してください");
		}

		if (StringUtils.isEmpty(login_id) == true) {
			messages.add("アカウント名を入力してください");
		}
		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		}

		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}